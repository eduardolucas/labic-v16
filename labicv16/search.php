<?php get_header(); ?>

  <section class="section-body">
    <div class="container page-container">

          <header>
            <h1 class="page-title">
              <?php echo sprintf( __( '%s resultados da busca por: ', 'labicv16' ), $wp_query->found_posts ); echo get_search_query(); ?>
            </h1>
            <hr>
          </header>

          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <article id="post-<?php the_ID(); ?>" class="clearfix single-content">

            <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-4 col-md-3 img-responsive')); ?></a>

            <section>
              <h2 class="last-posts-title">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
              </h2>

              <h5 class="post-datetime">
                <?php the_time('j \d\e F \d\e Y'); ?>
                •
                por <?php
                          if ( function_exists(
                            'coauthors' ) ) {
                            coauthors(); }
                          else {
                            the_author();
                          } ?>
              </h5>

              <?php the_excerpt(); ?>

            </section>

            <div class="post-divider">
              <a href="<?php the_permalink(); ?>">Leia Mais...</a>
            </div>

          </article>

          <?php endwhile; ?>
          <?php else : ?>

          <p>
            <?php _e("Nada encontrado", "labicv16"); ?>
          </p>

          <?php endif; ?>

          <div class="text-center pagenavi-single">
              <?php wp_pagenavi(); ?>
          </div>
    </div><!-- end .container -->
  </section>

<?php get_footer(); ?>
