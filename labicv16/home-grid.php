

<article class="col-xs-6 col-sm-3 col-md-6 home-post">
  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <a href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail( 'home-th-grid', array('class' => 'video-post-th img-responsive')); ?>
    </a>

    <p class="video-post-title">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
      <?php the_title(); ?>
      </a>
    </p>

  </div>
</article>
