<?php get_header(); ?>

  <!-- get_template_part( 'video', 'live' );  -->

  <section class="container">
    <div class="section-body home-body">

    <div class="row">
      <section class="col-xs-12 col-sm-12 col-md-8 section-bottom-spacer featured-home">
        <?php echo do_shortcode('[wp-tiles post_type="cartografia" orderby="date" order="DESC" grids="Featured Cartography" pagination="none"]'); ?>
        <!-- <img src="http://placehold.it/750x400" alt="" class="featured-img-home"> -->
      </section>

      <aside class="col-xs-12 col-sm-12 col-md-4 aside-home aside-text">
        <?php
              $count=0;
              query_posts( array (
              'post_type' => 'cartografia',
              'posts_per_page' => 4,
              'offset' => 1 ) );?>

        <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part( 'home', 'grid' ); ?>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
      </aside>
    </div>

    <div class="row">
      <section class="col-xs-12 col-sm-8">

        <?php $args = array( 'posts_per_page' => 4 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <article id="post-<?php the_ID(); ?>" class="post-home col-xs-12 bottom-spacer">

              <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-3 col-sm-4 col-md-3 img-responsive')); ?></a>

              <section>
                <h2 class="last-posts-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                </h2>

                <h5 class="post-datetime">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                  •
                  por <?php
                            if ( function_exists(
                              'coauthors' ) ) {
                              coauthors(); }
                            else {
                              the_author();
                            } ?>
                </h5>
              </section>

              <div class="post-divider">
                <a href="<?php the_permalink(); ?>">Leia Mais...</a>
              </div>

            </article>

        <?php endwhile; ?>

      </section>

      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'home_sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'home_sidebar' ); ?>
        <?php endif; ?>
      </aside>

    </div><!-- row -->

    </div>
  </section>



<?php get_footer(); ?>
