<?php get_header(); ?>

  <section class="container section-body">
    <div class="row">
      <section class="col-xs-12 col-sm-8">

        <h1 class="category-title-divider text-left"><a>
          <?php post_type_archive_title(); ?>
        </a></h1>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" class="post-home col-xs-12 bottom-spacer">
              <header>
                <h2 class="last-posts-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                </h2>

                <h5 class="post-datetime">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                </h5>

                <h6 class="post-datetime">
                por <?php
                          if ( function_exists(
                            'coauthors' ) ) {
                            coauthors(); }
                          else {
                            the_author();
                          } ?>
                </h6>
              </header>

              <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-4 col-sm-3 img-responsive')); ?></a>

              <section class="last-posts-body">
                <?php the_excerpt(); ?>
              </section>

              <div class="post-divider">
                <a href="<?php the_permalink(); ?>">Leia Mais...</a>
              </div>
            </article>

        <?php endwhile; ?>
        <?php endif; ?>

        <div class="text-center pagenavi-single">
            <?php wp_pagenavi(); ?>
        </div>

      </section>

      <aside class="col-xs-12 col-sm-4">
          <?php if ( is_active_sidebar( 'chart_sidebar' ) ) : ?>
          <?php dynamic_sidebar( 'chart_sidebar' ); ?>
          <?php endif; ?>
      </aside>
    </div><!-- row -->
  </section><!--container -->

  <div class="container archive-body">
    <div class="row">
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_left' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_left' ); ?>
        <?php endif; ?>
      </aside>
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_center' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_center' ); ?>
        <?php endif; ?>
      </aside>
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_right' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_right' ); ?>
        <?php endif; ?>
      </aside>
    </div><!-- row -->
  </div><!--container -->


<?php get_footer(); ?>
