<?php
      query_posts( array (
      'post_type' => 'video',
      'categoria-de-video' => 'aovivo',
      'posts_per_page' => 1 ) );?>

<?php while (have_posts()) : the_post(); ?>

<section class="live-featured">
  <div class="container-fluid">
    <header class="row">
      <section class="col-xs-12 col-sm-8 live-video-title">
        <h3 class="text-uppercase page-title">
          <span class="label label-danger">AO VIVO</span>
          <?php the_title(); ?>
        </h3>
      </section>
      <aside class="col-xs-12 col-sm-4 text-right live-share-aside">
        <!--ul class="list-inline">
          <li>
            <h6>Compartilhe esta transmissão:</h6>
          </li>
          <li-->
            <!-- ?php if ( function_exists( 'sharing_display' ) ) {
                sharing_display( '', true );
              }

            if ( class_exists( 'Jetpack_Likes' ) ) {
                $custom_likes = new Jetpack_Likes;
                echo $custom_likes->post_likes( '' );
              }
            ? -->
          <!--/li>
        </ul-->
      </aside>
    </header>
    <div class="row">
      <section class="col-xs-12 live-video">
        <iframe class="live-embed" width="848" height="480" src="<?php echo get_post_meta($post->ID, 'wpcf-embed-url', TRUE); ?>" frameborder="0" allowfullscreen></iframe>
      </section>
      <aside class="col-xs-12 live-aside">
        <div class="live-box-aside">
          <header class="live-box-description clearfix">
            <?php the_post_thumbnail( 'live-thumb', array('class' => 'live-th img-thumbnail')); ?>
            <div class="last-posts-body">
            <?php the_excerpt(); ?>
            </div>
          </header>
          <section class="live-box-section">
            <?php the_content(); ?>
          </section>
        </div>
      </aside>
    </div>
  </div>
</section>

<?php endwhile; ?>
<?php wp_reset_query(); ?>
