  <aside class="col-xs-12 col-sm-3 col-md-offset-1 aside-bar aside-pub-bar">

      <section class="pub-meta-item pub-meta-tags">
        <h4>
        <i class="fa fa-hashtag fa-1x pull-left" aria-hidden="true"></i> Palavras-Chave
        </h4>

        <?php
           if ( function_exists( 'the_terms' ) ) {
             the_terms( $post->ID, 'palavra-chave', '', '' );
           }
           else {
           }
           ?>


      </section>
      <section class="pub-meta-item pub-meta-year">
        <h4><i class="fa fa-calendar fa-1x pull-left" aria-hidden="true"></i>
          Ano
        </h4>

        <h4>
          <?php echo the_time('Y'); ?>
        </h4>
      </section>
      <section class="pub-meta-item pub-meta-download">
        <h4>
        <i class="fa fa-download fa-1x pull-left" aria-hidden="true"></i> Download
        </h4>

        <a target="_blank" href="<?php echo get_post_meta($post->ID, 'wpcf-url-publ', TRUE); ?>"><div class="file-icon file-icon-default" data-type="<?php echo get_post_meta($post->ID, 'wpcf-file-type', TRUE); ?>"></div></a>
      </section>
  </aside>
