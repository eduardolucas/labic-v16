<?php get_header(); ?>

  <!-- get_template_part( 'video', 'live' );  -->

  <section class="container-fluid section-body">

    <h1 class="category-title-divider text-left"><a>
      Vídeos • <?php echo get_queried_object()->name; ?></a>
    </h1>

    <div class="row">

      <aside class="col-xs-12 col-sm-3">

        <div class="aside-bar social-bar aside-widget affix-video" id="nav" data-spy="affix" data-offset-top="110">

          <h5 class="text-center text-uppercase">
            <strong>
              Categorias
            </strong>
          </h5>
          <hr>

          <?php if ( has_nav_menu( 'menuvideo' ) ) : ?>
            <?php wp_nav_menu( array(
             'theme_location' => 'menuvideo',
             'link_before' => '',
             'link_after' => '',
             'menu_class' => 'nav nav-pills nav-stacked',
             'fallback_cb' => false ) );
            ?>
          <?php endif; ?>
        </div>

      </aside>

      <section  class="col-xs-12 col-sm-9">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php get_template_part( 'video', 'grid' ); ?>
        <?php endwhile; ?>
        <?php endif; ?>

      </section>

    </div><!-- row -->
  </section><!--container -->

<?php get_footer(); ?>
