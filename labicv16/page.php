<?php get_header(); ?>

      <section class="container page-container section-body">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <header>
          <h1 class="page-title-divider text-left">
            <a>
            <?php the_title(); ?>
            </a>
          </h1>
        </header>

        <!-- article -->
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
        </article>
        <!-- /article -->
        <?php endwhile; ?>
        <?php endif; ?>

      </section>

<?php get_footer(); ?>
