// header shrinks when scroll page

jQuery(window).on("scroll touchmove", function () {
  jQuery('#top_bar').toggleClass('tiny', jQuery(document).scrollTop() > 200);

  jQuery('#mega-menu-wrap-primary').toggleClass('tiny', jQuery(document).scrollTop() > 200);
});

jQuery('#nav').affix({
    offset: {
        top: jQuery('#nav').offset().top,
        bottom: jQuery('footer').outerHeight(true) + jQuery('.application').outerHeight(true) + 110
    }
});

function goBack() {
    window.history.back();
}
