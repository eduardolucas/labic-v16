  <aside class="col-xs-12 col-sm-3 col-md-offset-1 aside-bar aside-pub-bar">

      <section class="pub-meta-item pub-meta-tags">
        <h4>
          Descrição
        </h4>

        <p>
          <?php the_excerpt(); ?>
        </p>

        <?php
           if ( function_exists( 'the_terms' ) ) {
             the_terms( $post->ID, 'palavra-chave', '', '' );
           }
           else {
           }
           ?>


      </section>
      <section class="pub-meta-item pub-meta-year">
        <h4>
          Duração
        </h4>

        <h5>
          <i class="fa fa-clock-o" aria-hidden="true"></i>
          <?php echo get_post_meta($post->ID, 'wpcf-video-time', TRUE); ?>
        </h5>
      </section>
      <section class="pub-meta-item pub-meta-download">
        <h4>
          Licença
        </h4>
        <h5>
          <?php echo get_post_meta($post->ID, 'wpcf-video-license', TRUE); ?>
        </h5>
      </section>
  </aside>
