<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php wp_title(''); ?> </title>
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/apple-touch-icon.png">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.png">
    <!--[if IE]>
      <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.ico">
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  <?php wp_head(); ?>
  </head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".scrollspy" data-offset-top="70">

  <nav id="top_bar" class="navbar navbar-fixed-top container-fluid header-navbar">
    <div class="navbar-header pull-left">
      <a class="navbar-brand" href="<?php echo home_url(); ?>">
        <div class="header-logo"></div>

      </a>
    </div>

    <ul class="nav navbar-nav navbar-right list-inline">
      <li>
        <div id="sb-search" class="sb-search">
          <?php get_search_form(); ?>
        </div>
      </li>
      <li class="hidden-xs">
         <a target="_blank" href="#">
         <span class="fa-stack fa-lg">
         <!-- <i class="fa fa-square fa-stack-2x"></i> -->
         <i class="fa fa-facebook fa-stack-1x" aria-hidden="true"></i>
         </span>
         </a>
      </li>
      <li class="hidden-xs">
         <a target="_blank" href="#">
         <span class="fa-stack fa-lg">
         <!-- <i class="fa fa-circle fa-stack-2x"></i> -->
         <i class="fa fa-twitter fa-stack-1x" aria-hidden="true"></i>
         </span>
         </a>
      </li>
      <li class="hidden-xs">
         <a target="_blank" href="#">
         <span class="fa-stack fa-lg">
         <!-- <i class="fa fa-square fa-stack-2x"></i> -->
         <i class="fa fa-github fa-stack-1x" aria-hidden="true"></i>
         </span>
         </a>
      </li>
    </ul>


  </nav>

  <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
