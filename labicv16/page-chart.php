<?php /* Template Name: Chart Page */ get_header(); ?>

  <header class="featured-tiles">
    <?php echo do_shortcode('[wp-tiles post_type="cartografia" orderby="date" order="DESC" grids="Featured Cartography" pagination="none"]'); ?>
  </header>

  <section class="container section-body">
    <div class="row">
      <section class="col-xs-12 col-sm-8 section-bottom-spacer">

        <?php $args = array( 'post_type' => 'cartografia', 'posts_per_page' => 4, 'offset' => 1 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <article id="post-<?php the_ID(); ?>" class="post-home col-xs-12 bottom-spacer">
              <header>
                <h2 class="last-posts-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                </h2>

                <h5 class="post-datetime">
                  <?php the_time('j \d\e F \d\e Y'); ?>
                </h5>

                <h6 class="post-datetime">
                por <?php
                          if ( function_exists(
                            'coauthors' ) ) {
                            coauthors(); }
                          else {
                            the_author();
                          } ?>
                </h6>
              </header>

              <a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-4 col-sm-3 img-responsive')); ?></a>

              <section class="last-posts-body">
                <?php the_excerpt(); ?>
              </section>

              <div class="post-divider">
                <a href="<?php the_permalink(); ?>">Leia Mais...</a>
              </div>
            </article>

        <?php endwhile; ?>

        <div class="text-center pagenavi-single">
          <a class="col-xs-12 btn btn-default text-uppercase btn-page-text btn-page" href="<?php echo home_url(); ?>/cartografia" role="button">Ver tudo</a>
        </div>

      </section>

      <aside class="col-xs-12 col-sm-4">
          <?php if ( is_active_sidebar( 'chart_sidebar' ) ) : ?>
          <?php dynamic_sidebar( 'chart_sidebar' ); ?>
          <?php endif; ?>
      </aside>
    </div><!-- row -->
  </section><!--container -->

  <div class="container archive-body">
    <div class="row">
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_left' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_left' ); ?>
        <?php endif; ?>
      </aside>
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_center' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_center' ); ?>
        <?php endif; ?>
      </aside>
      <aside class="col-xs-12 col-sm-4">
        <?php if ( is_active_sidebar( 'chart_sidebar_right' ) ) : ?>
        <?php dynamic_sidebar( 'chart_sidebar_right' ); ?>
        <?php endif; ?>
      </aside>
    </div><!-- row -->
  </div><!--container -->


<?php get_footer(); ?>
