<?php
/**
 * labicv16 1.0 Theme Customizer support
 *
 * @package WordPress
 * @subpackage labicv16
 */

function labicv16_customize_register( $wp_customize ) {

	// Opções Gerais

	$wp_customize->add_panel( 'labicv16_live_panel', array(
		'priority'       => 250,
		'capability'     => 'edit_theme_options',
		'title'          => __( 'Ao Vivo - Configurações' , 'labicv16'),
		'description'    => __( 'Configure aqui as opções de ao vivo do seu site.' , 'labicv16')
	) );


  // Liga/desliga Ao vivo

		$wp_customize->add_section( 'labicv16_general_live', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Liga/Desliga Ao Vivo' , 'labicv16'),
			'description'    => __( 'Liga ou desliga a área de transmissão ao vivo.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );
    $wp_customize->add_setting('labicv16_live_selector', array(
            'default'        => 'value2',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));
    $wp_customize->add_control('labicv16_live_selector', array(
        'label'      => __('Liga/Desliga Ao Vivo', 'labicv16'),
        'section'    => 'labicv16_general_live',
        'settings'   => 'labicv16_live_selector',
        'type'       => 'radio',
        'choices'    => array(
            'value1' => 'Ligado',
            'value2' => 'Desligado',
        ),
    ));


		// Texto botão galeria
		$wp_customize->add_section( 'labicv16_general_gallerytext', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Texto do botão da Galeria' , 'labicv16'),
			'description'    => __( 'Insira o texto a ser exibido no botão da Galeria.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );

		$wp_customize->add_setting( 'labicv16_gallerytext', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'labicv16_gallerytext',
			array(
				'label'      => 'Texto do botão',
				'section'    => 'labicv16_general_gallerytext',
				'type'       => 'text',
			)
		);

		// URL botão galeria
		$wp_customize->add_section( 'labicv16_general_galleryurl', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'URL do botão da Galeria' , 'labicv16'),
			'description'    => __( 'Insira o url do botão da Galeria.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );

		$wp_customize->add_setting( 'labicv16_galleryurl', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'labicv16_galleryurl',
			array(
				'label'      => 'URL do botão',
				'section'    => 'labicv16_general_galleryurl',
				'type'       => 'text',
			)
		);

		// Mapa
		$wp_customize->add_section( 'labicv16_general_map', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Código do Mapa' , 'labicv16'),
			'description'    => __( 'Insira o código de incorporação (embed code) do Mapa.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );

		$wp_customize->add_setting( 'labicv16_map', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'labicv16_map',
			array(
				'label'      => 'Código do mapa',
				'section'    => 'labicv16_general_map',
				'type'       => 'text',
			)
		);

		// Contatos
		$wp_customize->add_section( 'labicv16_general_contact', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Título Seção Contatos' , 'labicv16'),
			'description'    => __( 'Insira o título da seção Contatos.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );

		$wp_customize->add_setting( 'labicv16_contact', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'labicv16_contact',
			array(
				'label'      => 'Título da seção',
				'section'    => 'labicv16_general_contact',
				'type'       => 'text',
			)
		);

		// Copyright
		$wp_customize->add_section( 'labicv16_general_copyright', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'title'          => __( 'Texto Copyright' , 'labicv16'),
			'description'    => __( 'Insira o texto de Copyright.' , 'labicv16'),
			'panel'          => 'labicv16_live_panel'
		) );

		$wp_customize->add_setting( 'labicv16_copyright', array( 'sanitize_callback' => 'sanitize_text_field' ) );

		$wp_customize->add_control(
			'labicv16_copyright',
			array(
				'label'      => 'Texto de Copyright',
				'section'    => 'labicv16_general_copyright',
				'type'       => 'text',
			)
		);

}

add_action( 'customize_register', 'labicv16_customize_register' );

if ( class_exists( 'WP_Customize_Section' ) && !class_exists( 'labicv16_Customized_Section' ) ) {
	class labicv16_Customized_Section extends WP_Customize_Section {
		public function render() {
			$classes = 'accordion-section control-section control-section-' . $this->type;
			?>
			<li id="accordion-section-<?php echo esc_attr( $this->id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
				<style type="text/css">
					.cohhe-social-profiles {
						padding: 14px;
					}
					.cohhe-social-profiles li:last-child {
						display: none !important;
					}
					.cohhe-social-profiles li i {
						width: 20px;
						height: 20px;
						display: inline-block;
						background-size: cover !important;
						margin-right: 5px;
						float: left;
					}
					.cohhe-social-profiles li i.cohhe_logo {
						background: url(<?php echo get_template_directory_uri().'/images/icons/cohhe.png'; ?>);
					}
					.cohhe-social-profiles li a {
						height: 20px;
						line-height: 20px;
					}
					#customize-theme-controls>ul>#accordion-section-longform_social_links {
						margin-top: 10px;
					}
					.cohhe-social-profiles li.documentation {
						text-align: right;
						margin-bottom: 10px;
					}
					.cohhe-social-profiles li.gopremium {
						text-align: right;
						margin-bottom: 60px;
					}
				</style>
			</li>
			<?php
		}
	}
}


function labicv16_sanitize_checkbox( $input ) {
	// Boolean check
	return ( ( isset( $input ) && true == $input ) ? true : false );
}

/*
 * Bind JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function labicv16_customize_preview_js() {
	wp_enqueue_script( 'labicv16_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20131205', true );
}
add_action( 'customize_preview_init', 'labicv16_customize_preview_js' );
