<?php /* Template Name: Team Page */ get_header(); ?>

      <section class="section-body">
        <div class="container-fluid">

          <h1 class="category-title-divider text-left">
            <a>
              <?php the_title(); ?>
            </a>
          </h1>

          <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <!-- article -->
          <article id="post-<?php the_ID(); ?>"
          <?php post_class(); ?>>
          <?php the_content(); ?>
          </article>
          <!-- /article -->

          <?php echo apply_filters('the_content', get_post_meta($post->ID, 'wpcf-tiled-bottom', true)); ?>

          <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </section>

<?php get_footer(); ?>
