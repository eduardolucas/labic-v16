<?php
/*
 * Theme Name: labicv16
 * Theme URI: http://labic.net
 * Author: Eduardo Lucas
 * Author URI: eduardolucas.biz
 * Labic Theme functions
 */

// Load scripts

function labicv16_header_scripts() {

    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
    wp_enqueue_script('bootstrap-js');

    wp_register_script('labicv16js', get_template_directory_uri() . '/js/script.js', array('jquery'), '0.1', true);
    wp_enqueue_script('labicv16js');
}

// Load styles

function labicv16_styles() {

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '', 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '', 'all');
    wp_enqueue_style('fontawesome');

    wp_register_style('fileicon', get_template_directory_uri() . '/css/fileicon.css', array(), '', 'all');
    wp_enqueue_style('fileicon');

    wp_register_style('labicv16', get_template_directory_uri() . '/style.css', array(), '', 'all');
    wp_enqueue_style('labicv16');
}

// Thumbnails support

if ( function_exists( 'add_theme_support' ) ) {
    // Add Menu Support
    add_theme_support('menus');

  	add_theme_support( 'post-thumbnails' );
	// set_post_thumbnail_size( 406, 281, true );
  add_image_size('xtra-large', '', 720, true); // Xtra Large Thumbnail
  add_image_size('large', 700, '', true); // Large Thumbnail
  add_image_size('medium', 250, '', true); // Medium Thumbnail
  add_image_size('small', 120, '', true); // Small Thumbnail
  add_image_size('home-thumb', '', 96, true); //width, height
  add_image_size('home-th-grid', 150, 100, true); //width, height
	add_image_size('publ-thumb', 64, 64, true); //width, height
  add_image_size('video-thumb', 220, 124, true ); //width, height
  add_image_size('live-thumb', '', 72, true ); //width, height
}

// Register menu

register_nav_menus( array(
    'primary' => __( 'Menu Principal', 'labicv16' ),
) );

register_nav_menus( array(
    'menuabout' => __( 'Menu TOC About', 'labicv16' ),
) );

register_nav_menus( array(
    'menuaboutpesquisas' => __( 'Menu TOC About Pesquisas', 'labicv16' ),
) );

register_nav_menus( array(
    'menuaboutprojetos' => __( 'Menu TOC About Projetos', 'labicv16' ),
) );

register_nav_menus( array(
    'menuvideo' => __( 'Menu Video Categories', 'labicv16' ),
) );

// Register Widgets

function labicv16_widgets_init() {

  register_sidebar( array(
		'name'          => 'Barra Lateral Principal',
		'id'            => 'home_sidebar',
		'before_widget' => '<div class="aside-bar social-bar aside-widget">',
		'after_widget'  => '</p>
  </div>',
		'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
		'after_title'   => '</strong></h5><hr>
    <p>',
	) );

  register_sidebar( array(
		'name'          => 'Barra Lateral - Categorias',
		'id'            => 'category_sidebar',
		'before_widget' => '<div class="aside-bar social-bar aside-widget">',
		'after_widget'  => '</p>
  </div>',
		'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
		'after_title'   => '</strong></h5><hr>
    <p>',
	) );

  register_sidebar( array(
		'name'          => 'Blog - Barra inferior esquerda',
		'id'            => 'blog_sidebar_left',
		'before_widget' => '<div class="aside-bar social-bar aside-widget">',
		'after_widget'  => '</p>
  </div>',
		'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
		'after_title'   => '</strong></h5><hr>
    <p>',
	) );

  register_sidebar( array(
    'name'          => 'Blog - Barra inferior central',
    'id'            => 'blog_sidebar_center',
    'before_widget' => '<div class="aside-bar social-bar aside-widget">',
    'after_widget'  => '</p>
  </div>',
    'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
    'after_title'   => '</strong></h5><hr>
    <p>',
  ) );

  register_sidebar( array(
    'name'          => 'Blog - Barra inferior direita',
    'id'            => 'blog_sidebar_right',
    'before_widget' => '<div class="aside-bar social-bar aside-widget">',
    'after_widget'  => '</p>
  </div>',
    'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
    'after_title'   => '</strong></h5><hr>
    <p>',
  ) );

  register_sidebar( array(
		'name'          => 'Cartografias - Barra Lateral',
		'id'            => 'chart_sidebar',
		'before_widget' => '<div class="aside-bar social-bar aside-widget">',
		'after_widget'  => '</p>
  </div>',
		'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
		'after_title'   => '</strong></h5><hr>
    <p>',
	) );

  register_sidebar( array(
    'name'          => 'Cartografias - Barra inferior esquerda',
    'id'            => 'chart_sidebar_left',
    'before_widget' => '<div class="aside-bar social-bar aside-widget">',
    'after_widget'  => '</p>
  </div>',
    'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
    'after_title'   => '</strong></h5><hr>
    <p>',
  ) );

  register_sidebar( array(
    'name'          => 'Cartografias - Barra inferior central',
    'id'            => 'chart_sidebar_center',
    'before_widget' => '<div class="aside-bar social-bar aside-widget">',
    'after_widget'  => '</p>
  </div>',
    'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
    'after_title'   => '</strong></h5><hr>
    <p>',
  ) );

  register_sidebar( array(
    'name'          => 'Cartografias - Barra inferior direita',
    'id'            => 'chart_sidebar_right',
    'before_widget' => '<div class="aside-bar social-bar aside-widget">',
    'after_widget'  => '</p>
  </div>',
    'before_title'  => '<h5 class="text-center text-uppercase"><strong>',
    'after_title'   => '</strong></h5><hr>
    <p>',
  ) );

}

// ShortCodes Support

function gexf_function($atts, $content = null) {
   extract(shortcode_atts(array(
      "src" => ''
   ), $atts));
   return '<iframe class="wide" frameborder="0" width="'.$width.'" height="'.$height.'" src="https://dl.dropboxusercontent.com/u/23392158/embed-gexf-labic/index.html#../gexf-files/'.$src.'" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>';
}

function cartodb_function($atts, $content = null) {
   extract(shortcode_atts(array(
      "src" => ''
   ), $atts));
   return '<iframe class="wide" frameborder="0" width="'.$width.'" height="'.$height.'" src="'.$src.'" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>';
}

function storymap_function($atts, $content = null) {
   extract(shortcode_atts(array(
      "src" => ''
   ), $atts));
   return '<iframe class="wide" frameborder="0" width="'.$width.'" height="'.$height.'" src="'.$src.'" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>';
}

function legenda_shortcode( $atts, $content = null ) {
	return '<h4 style="color: #808080;" class="text-center"><em>' . $content . '</em></h4><hr>';
}

// other functions

function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}

// Add Theme Customizer functionality
require get_template_directory() . '/inc/customizer.php';

// Admin Javascript
add_action( 'admin_enqueue_scripts', 'labicv16_admin_scripts' );
function labicv16_admin_scripts() {
	wp_register_script('master', get_template_directory_uri() . '/js/admin-master.js', array('jquery'));
	wp_enqueue_script('master');
}

// Add Actions

add_action( 'init', 'labicv16_header_scripts' );
add_action( 'wp_enqueue_scripts', 'labicv16_styles' );
add_action( 'widgets_init', 'labicv16_widgets_init' );
add_action( 'loop_start', 'jptweak_remove_share' );
add_shortcode("gexf", "gexf_function");
add_shortcode("cartodb", "cartodb_function");
add_shortcode("storymap", "storymap_function");
add_shortcode( 'legenda', 'legenda_shortcode' );

 ?>
