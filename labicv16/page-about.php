<?php /* Template Name: About Page */ get_header(); ?>

      <?php $menuid = get_post_meta($post->ID, 'menuslug', TRUE); ?>

      <section class="section-body">

        <div class="container">

          <header>
            <h1 class="page-title-divider text-left">
              <a>
              <?php the_title(); ?>
              </a>
            </h1>
          </header>

            <div class="row">

              <aside class="col-xs-12 col-sm-4 scrollspy">
                <div class="aside-bar social-bar aside-widget affix-about" id="nav" data-spy="affix">

                  <h5 class="text-center text-uppercase">
                    <strong>
                      <?php the_title(); ?>
                    </strong>
                  </h5>

                  <hr>

                  <?php if ( has_nav_menu( $menuid ) ) : ?>
                    <?php wp_nav_menu( array(
                     'theme_location' => $menuid,
                     'link_before' => '',
                     'link_after' => '',
                     'menu_class' => 'nav nav-pills nav-stacked',
                     'fallback_cb' => false ) );
                    ?>
                  <?php endif; ?>

                </div>
              </aside>

              <section class="col-xs-12 col-sm-8">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                  <?php the_content(); ?>
                </article>
                <!-- /article -->
                <?php endwhile; ?>
                <?php endif; ?>
              </section>

            </div><!--end of .row-->
          </div><!--end of .container-->

          <section class="container section-body">
            <div class="row">
              <aside class="col-xs-12 col-sm-4 col-sm-offset-4">
                <?php if ( is_active_sidebar( 'blog_sidebar_left' ) ) : ?>
                <?php dynamic_sidebar( 'blog_sidebar_left' ); ?>
                <?php endif; ?>
              </aside>
              <aside class="col-xs-12 col-sm-4">
                <?php if ( is_active_sidebar( 'blog_sidebar_center' ) ) : ?>
                <?php dynamic_sidebar( 'blog_sidebar_center' ); ?>
                <?php endif; ?>
              </aside>
            </div><!-- row -->
          </section><!--container -->


      </section>

<?php get_footer(); ?>
