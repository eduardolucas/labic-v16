  <footer class="footer-bottom">
    <hr class="center-block">
    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img class="center-block" alt="creative commons-by-sa" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/cc-by-sa.png"></a>
  </footer>

  <?php wp_footer(); ?>

</body>
</html>
