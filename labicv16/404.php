<?php get_header(); ?>

  <section class="text-center page-404">
    <div class="container">
      <div class="row">

          <section class="col-xs-12 col-sm-6 hidden-xs">
            <img src="https://m.popkey.co/cedcc1/eLw7m.gif" alt="" />
          </section>

          <section class="col-xs-12 col-sm-6">
            <h1>404</h1>

            <h2 class="section-bottom-spacer">Página não encontrada</h2>

            <button onclick="goBack()" class="btn btn-default" role="button"><i class="fa fa-arrow-left" aria-hidden="true"></i>
            Voltar</button>

            <a class="btn btn-default" href="<?php echo home_url(); ?>" role="button"><i class="fa fa-home" aria-hidden="true"></i>
            Página Inicial</a>
          </section>

      </div> <!--row -->
    </div> <!--container -->
  </section>

<?php get_footer(); ?>
