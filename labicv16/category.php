<?php get_header(); ?>

  <section class="container section-body">

    <h1 class="category-title-divider text-left"><a>
      <?php printf( __( '', 'labicv16' ), single_cat_title( '', true ) ); ?>
    </a></h1>

    <div class="row">
      <section class="col-xs-12 col-sm-8">

      <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" class="bottom-spacer">
          <h2 class="last-posts-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
          </h2>

          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail( 'home-thumb', array('class' => 'last-posts-th col-xs-12 col-sm-3')); ?>
          </a>

          <h5 class="post-datetime">
            <?php the_time('j \d\e F \d\e Y'); ?>
          </h5>

          <h6 class="post-datetime">
          por <?php
                    if ( function_exists(
                      'coauthors' ) ) {
                      coauthors(); }
                    else {
                      the_author();
                    } ?>
          </h6>

          <section class="last-posts-body">
            <?php the_excerpt(); ?>
          </section>

          <div class="bottom-spacer">
            <a href="<?php the_permalink(); ?>">Leia Mais...</a>
          </div>
        </article>

      <?php endwhile; ?>
      <?php endif; ?>

      <div class="text-center pagenavi-single">
          <?php wp_pagenavi(); ?>
      </div>

      </section>

      <aside class="col-xs-12 col-sm-4">
          <?php if ( is_active_sidebar( 'category_sidebar' ) ) : ?>
          <?php dynamic_sidebar( 'category_sidebar' ); ?>
          <?php endif; ?>
      </aside>

    </div><!-- row -->
  </section><!--container -->

<?php get_footer(); ?>
