

<article class="col-xs-12 col-sm-4 col-lg-3 video-post">
  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <a href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail( 'video-thumb', array('class' => 'video-post-th img-responsive col-xs-6 col-sm-12')); ?>
    </a>

    <p class="video-post-title">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
      <?php the_title(); ?>
      </a>
    </p>

  </div>
</article>
