<?php get_header(); ?>


  <section class="section-body">
      <div class="container">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="row">

          <section class="col-xs-12 col-sm-8">

            <header>
              <h1 class="page-title">
                <?php the_title(); ?>
              </h1>
              <hr>
            </header>

            <article class="clearfix single-content">

              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?php echo get_post_meta($post->ID, 'wpcf-embed-url', TRUE); ?>" frameborder="0" allowfullscreen></iframe>
              </div>

              <?php the_content(); ?>

            </article>

          </section>

          <?php get_sidebar('video'); ?>

        </div><!-- row -->

        <div class="row">
          <section class="col-xs-12 col-sm-8">

            <section class="clearfix">
              <h2>Compartilhe </h2>
              <div class="text-center social-bar aside-bar">
              <?php if ( function_exists( 'sharing_display' ) ) {
                  sharing_display( '', true );
                }

              if ( class_exists( 'Jetpack_Likes' ) ) {
                  $custom_likes = new Jetpack_Likes;
                  echo $custom_likes->post_likes( '' );
                }
              ?>
              </div>
            </section>

          </section>
        </div>

      </div><!-- end .container -->

        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>

  </section>


<?php get_footer(); ?>
