<?php get_header(); ?>

<section class="section-body">
  <div class="container single-container">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article>

        <header>
          <h1 class="page-title">
            <?php the_title(); ?>
          </h1>

          <h5 class="single-datetime">
            <?php the_time('j \d\e F \d\e Y'); ?>
          </h5>

          <h4 class="single-author">por <?php
                  if ( function_exists(
                    'coauthors' ) ) {
                    coauthors(); }
                  else {
                    the_author();
                  } ?>
          </h4>
          <hr>
        </header>

        <article class="clearfix single-content">
          <?php the_content(); ?>
        </article>

        <section class="clearfix">
          <h2>Compartilhe </h2>
          <div class="text-center social-bar aside-bar">
          <?php if ( function_exists( 'sharing_display' ) ) {
              sharing_display( '', true );
            }

          if ( class_exists( 'Jetpack_Likes' ) ) {
              $custom_likes = new Jetpack_Likes;
              echo $custom_likes->post_likes( '' );
            }
          ?>
          </div>

        </section>

    </article>

  </div><!-- end .container -->

  <section class="comments-section">
    <div class="container single-container">

      <div class="clearfix">
        <h2>Comentários</h2>
      </div>

      <!-- ?php comments_template(); ?-->

    </div>
  </section>

    <?php endwhile; ?>
    <?php else : ?>
    <?php endif; ?>

</section>


<?php get_footer(); ?>
